package database.cinema.rest;

import database.cinema.domain.Cinema;
import database.cinema.service.CinemaService;
import database.token.security.CheckSecurity;
import database.util.RestUtilities;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("service/cinema")
public class CinemaController {

    private final CinemaService service;

    private final RestUtilities restUtilities;

    public CinemaController(CinemaService service, RestUtilities restUtilities) {
        this.service = service;
        this.restUtilities = restUtilities;
    }

    @PostMapping("/save")
    @CheckSecurity(roles = {"ADMIN"})
    @ApiOperation(value = "Saves the given cinema, if it already exists it will be updated")
    public ResponseEntity<?> saveOrUpdate(@RequestHeader("Authorization") String authorization,
                                          @Valid @RequestBody Cinema cinema, BindingResult result) {
        if (result.hasErrors())
            return restUtilities.createErrorMap(result);

        return new ResponseEntity<>(service.saveOrUpdate(cinema), HttpStatus.CREATED);
    }

    @DeleteMapping("/delete/{cinemaId}")
    @CheckSecurity(roles = {"ADMIN"})
    @ApiOperation(value = "Delete a cinema for the given cinema id")
    public ResponseEntity<?> deleteById(@RequestHeader("Authorization") String authorization,
                                        @PathVariable Long cinemaId) {
        service.deleteById(cinemaId);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/{cinemaId}")
    @CheckSecurity(roles = {"ADMIN", "USER"})
    @ApiOperation(value = "Find a cinema for the given cinema id")
    public ResponseEntity<Cinema> findById(@RequestHeader("Authorization") String authorization,
                                           @PathVariable Long cinemaId) {
        return service.findById(cinemaId)
                .map(cinema -> new ResponseEntity<>(cinema, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/all")
    @CheckSecurity(roles = {"ADMIN", "USER"})
    @ApiOperation(value = "Find all cinemas")
    public ResponseEntity<List<Cinema>> findAll(@RequestHeader("Authorization") String authorization) {
        return new ResponseEntity<>(service.findAll(), HttpStatus.OK);
    }

}
