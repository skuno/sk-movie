package database.cinema.service.impl;

import database.cinema.domain.Cinema;
import database.cinema.repository.CinemaRepository;
import database.cinema.service.CinemaService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CinemaServiceImpl implements CinemaService {

    private final CinemaRepository repository;

    public CinemaServiceImpl(CinemaRepository repository) {
        this.repository = repository;
    }

    @Override
    public Cinema saveOrUpdate(Cinema cinema) {
        return repository.save(cinema);
    }

    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<Cinema> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public List<Cinema> findAll() {
        return repository.findAll();
    }
}
