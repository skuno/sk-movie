package database.cinema.service;

import database.cinema.domain.Cinema;

import java.util.List;
import java.util.Optional;

public interface CinemaService {

    Cinema saveOrUpdate(Cinema cinema);

    void deleteById(Long id);

    Optional<Cinema> findById(Long id);

    List<Cinema> findAll();

}
