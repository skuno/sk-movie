package database.movie.service;

import database.movie.domain.Movie;

import java.util.List;
import java.util.Optional;

public interface MovieService {

    Movie saveOrUpdate(Movie movie);

    void deleteById(Long id);

    Optional<Movie> findById(Long id);

    List<Movie> findAll();

}
