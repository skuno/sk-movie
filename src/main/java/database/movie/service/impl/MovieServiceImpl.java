package database.movie.service.impl;

import database.movie.domain.Movie;
import database.movie.repository.MovieRepository;
import database.movie.service.MovieService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MovieServiceImpl implements MovieService {

    private final MovieRepository repository;

    public MovieServiceImpl(MovieRepository repository) {
        this.repository = repository;
    }

    @Override
    public Movie saveOrUpdate(Movie movie) {
        if (movie.getGenre() == null || movie.getGenre().isEmpty())
            movie.setGenre("Not provided");

        if (movie.getDescription() == null || movie.getDescription().isEmpty())
            movie.setDescription("Not provided");

        return repository.save(movie);
    }

    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<Movie> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public List<Movie> findAll() {
        return repository.findAll();
    }
}
