package database.movie.rest;

import database.movie.domain.Movie;
import database.movie.service.MovieService;
import database.token.security.CheckSecurity;
import database.util.RestUtilities;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("service/movie")
public class MovieController {

    private final MovieService service;

    private final RestUtilities restUtilities;

    public MovieController(MovieService service, RestUtilities restUtilities) {
        this.service = service;
        this.restUtilities = restUtilities;
    }

    @PostMapping("/save")
    @CheckSecurity(roles = {"ADMIN"})
    @ApiOperation("Save the given movie, if it already exists it will be updated")
    public ResponseEntity<?> saveOrUpdate(@RequestHeader("Authorization") String authorization,
                                          @Valid @RequestBody Movie movie, BindingResult bindingResult) {
        if (bindingResult.hasErrors())
            return restUtilities.createErrorMap(bindingResult);

        return new ResponseEntity<>(service.saveOrUpdate(movie), HttpStatus.CREATED);
    }

    @DeleteMapping("/delete/{movieId}")
    @CheckSecurity(roles = {"ADMIN"})
    @ApiOperation("Delete a movie for the given id")
    public ResponseEntity<?> deleteById(@RequestHeader("Authorization") String authorization,
                                        @PathVariable Long movieId) {
        service.deleteById(movieId);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/{movieId}")
    @CheckSecurity(roles = {"ADMIN", "USER"})
    @ApiOperation("Find a movie for the given movie id")
    public ResponseEntity<Movie> findById(@RequestHeader("Authorization") String authorization,
                                          @PathVariable Long movieId) {
        return service.findById(movieId)
                .map(movie -> new ResponseEntity<>(movie, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/all")
    @CheckSecurity(roles = {"ADMIN", "USER"})
    @ApiOperation("Find all movies")
    public ResponseEntity<List<Movie>> findAll(@RequestHeader("Authorization") String authorization) {
        return new ResponseEntity<>(service.findAll(), HttpStatus.OK);
    }

}
