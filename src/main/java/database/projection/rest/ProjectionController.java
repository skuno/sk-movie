package database.projection.rest;

import database.projection.domain.Projection;
import database.projection.service.ProjectionService;
import database.token.security.CheckSecurity;
import database.util.RestUtilities;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("service/projection")
public class ProjectionController {

    private final ProjectionService service;

    private final RestUtilities restUtilities;

    public ProjectionController(ProjectionService service, RestUtilities restUtilities) {
        this.service = service;
        this.restUtilities = restUtilities;
    }

    @PostMapping("/save")
    @CheckSecurity(roles = {"ADMIN", "USER"})
    @ApiOperation("Save projection or update an existing one")
    public ResponseEntity<?> saveOrUpdate(@RequestHeader("Authorization") String authorization,
                                          @Valid @RequestBody Projection projection, BindingResult bindingResult) {
        if (bindingResult.hasErrors())
            return restUtilities.createErrorMap(bindingResult);

        if (projection.getMovie() == null || projection.getCinema() == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        return new ResponseEntity<>(service.saveOrUpdate(projection, projection.getMovie(), projection.getCinema()), HttpStatus.OK);
    }

    @DeleteMapping("/delete/{projectionId}")
    @CheckSecurity(roles = {"ADMIN"})
    @ApiOperation("Delete projection for the given projection id")
    public ResponseEntity<?> deleteById(@RequestHeader("Authorization") String authorization,
                                        @PathVariable Long projectionId) {
        service.deleteById(projectionId);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/{projectionId}")
    @CheckSecurity(roles = {"ADMIN", "USER"})
    @ApiOperation("Find projection for the given projection id")
    public ResponseEntity<Projection> findById(@RequestHeader("Authorization") String authorization,
                                               @PathVariable Long projectionId) {
        return service.findById(projectionId)
                .map(projection -> new ResponseEntity<>(projection, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    @GetMapping("/all")
    @CheckSecurity(roles = {"ADMIN", "USER"})
    @ApiOperation("Find all projections")
    public ResponseEntity<List<Projection>> findAll(@RequestHeader("Authorization") String authorization) {
        return new ResponseEntity<>(service.findAll(), HttpStatus.OK);
    }

    @GetMapping("/all/{movieId}/{cinemaId}")
    @CheckSecurity(roles = {"ADMIN", "USER"})
    @ApiOperation("Find all projections with the given movie and cinema id")
    public ResponseEntity<List<Projection>> findByMovieIdAndTheaterId(@RequestHeader("Authorization") String authorization,
                                                                      @PathVariable Long movieId,
                                                                      @PathVariable Long cinemaId) {
        return new ResponseEntity<>(service.findByMovieIdAndTheaterId(movieId, cinemaId), HttpStatus.OK);
    }

    @GetMapping("/all/{movieId}/{cinemaId}/{projectionDate}")
    @CheckSecurity(roles = {"ADMIN", "USER"})
    @ApiOperation("Find all projections with the given movie, cinema and projection date")
    public ResponseEntity<List<Projection>> findByMovieIdTheaterIdAndDate(@RequestHeader("Authorization") String authorization,
                                                                          @PathVariable Long movieId,
                                                                          @PathVariable Long cinemaId,
                                                                          @PathVariable String projectionDate) {
        return new ResponseEntity<>(service.findByMovieIdTheaterIdAndDate(movieId, cinemaId, projectionDate), HttpStatus.OK);
    }
}
