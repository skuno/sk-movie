package database.projection.service.impl;

import database.cinema.domain.Cinema;
import database.cinema.service.CinemaService;
import database.movie.domain.Movie;
import database.movie.service.MovieService;
import database.projection.domain.Projection;
import database.projection.repository.ProjectionRepository;
import database.projection.service.ProjectionService;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProjectionServiceImpl implements ProjectionService {

    private final ProjectionRepository repository;

    private final MovieService movieService;

    private final CinemaService cinemaService;

    public ProjectionServiceImpl(ProjectionRepository repository,
                                 MovieService movieService,
                                 CinemaService cinemaService) {
        this.repository = repository;
        this.movieService = movieService;
        this.cinemaService = cinemaService;
    }

    @Override
    public Projection saveOrUpdate(Projection projection, Movie movie, Cinema cinema) {
        if (projection.getNumberOfReservations() == null)
            projection.setNumberOfReservations(0);

        if (movieDoesntExist(movie))
            projection.setMovie(movieService.saveOrUpdate(movie));

        if (cinemaDoesntExist(cinema))
            projection.setCinema(cinemaService.saveOrUpdate(cinema));

        return repository.save(projection);
    }

    private boolean movieDoesntExist(Movie movie) {
        return movie == null || movie.getId() == null ||
                !movieService.findById(movie.getId()).isPresent();
    }

    private boolean cinemaDoesntExist(Cinema cinema) {
        return cinema == null || cinema.getId() == null ||
                !cinemaService.findById(cinema.getId()).isPresent();
    }

    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<Projection> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public List<Projection> findAll() {
        return repository.findAll();
    }

    @Override
    public List<Projection> findByMovieIdAndTheaterId(Long movieId, Long cinemaId) {
        return repository.findAll().stream()
                .filter(projection -> projectionFilter(projection, movieId, cinemaId))
                .collect(Collectors.toList());
    }

    private boolean projectionFilter(Projection projection, Long movieId, Long cinemaId) {
        return projection.getMovie().getId().equals(movieId) &&
                projection.getCinema().getId().equals(cinemaId);
    }

    @Override
    public List<Projection> findByMovieIdTheaterIdAndDate(Long movieId, Long cinemaId, String projectionDate) {
        return repository.findAll().stream()
                .filter(projection -> projectionFilterWithDate(projection, movieId, cinemaId, projectionDate))
                .collect(Collectors.toList());
    }

    private boolean projectionFilterWithDate(Projection projection, Long movieId, Long cinemaId, String projectionDate) {
        try {
            return projection.getMovie().getId().equals(movieId) &&
                    projection.getCinema().getId().equals(cinemaId) &&
                    projection.getStartDateAsDateObject()
                            .after(new SimpleDateFormat("dd-MM-yyyy HH:mm").parse(projectionDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return false;
    }
}
