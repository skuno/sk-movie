package database.projection.service;

import database.cinema.domain.Cinema;
import database.movie.domain.Movie;
import database.projection.domain.Projection;

import java.util.List;
import java.util.Optional;

public interface ProjectionService {

    Projection saveOrUpdate(Projection projection, Movie movie, Cinema cinema);

    void deleteById(Long id);

    Optional<Projection> findById(Long id);

    List<Projection> findAll();

    List<Projection> findByMovieIdAndTheaterId(Long movieId, Long theaterId);

    List<Projection> findByMovieIdTheaterIdAndDate(Long movieId, Long cinemaId, String projectionDate);

}
