package database.projection.domain;

public enum ProjectionStatus {

    HAS_ROOM, FULL

}
