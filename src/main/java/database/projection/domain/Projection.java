package database.projection.domain;

import database.cinema.domain.Cinema;
import database.movie.domain.Movie;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table(name = "projection")
@Data
@NoArgsConstructor
public class Projection implements Serializable {

    @Id
    @Column(name = "projection_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "movie")
    private Movie movie;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "cinema")
    private Cinema cinema;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    @NotNull
    @PositiveOrZero
    private Float ticketPrice;

    @NotNull
    @Enumerated(EnumType.STRING)
    private ProjectionStatus status;

    @PositiveOrZero
    private Integer numberOfReservations;

    public String getStartDate() {
        return new SimpleDateFormat("dd-MM-yyyy HH:mm").format(startDate);
    }

    public Date getStartDateAsDateObject() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        try {
            this.startDate = new SimpleDateFormat("dd-MM-yyyy HH:mm").parse(startDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

}
